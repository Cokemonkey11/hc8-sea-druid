
import sys
import textwrap
import re

TEAM_NAME          = "[i]This is Not a Team[/i]'s"
HERO_NAME          = "Sea Druid"
AUTHORS            = "Presented by Freddyk and Cokemonkey11"


INTRODUCTION = (

    """

        Sea Druid is a titanic water-breathing protector of the depths, which
        specializes in supporting friendly troops. His flexible combination of
        spells allows him to react to any scenario.

    """,

    """

        Sea Druid's medium-low cooldowns and spell-augmenting abilities make
        spell-weaving - attacking in between occasional spell casts - a powerful
        play style. This naturally opportunistic hero benefits from on-demand
        disrupts and an execute mechanic.

    """,

    """

        Sea Druid's few augment properties lead to an ability kit that's easy to
        learn but provides many possibilities to experiment with. For example,
        try casting Mass Brainwash with a large army outside of battle to
        guarantee the secondary attack-speed bonus, before sieging your
        opponent's base.

    """
)


DESCRIPTOR = (
    (
        "https://i.imgur.com/fGtjwx1.png",
        (
            "Sea Druid: Mystical Hero, adept at disrupting units and simultaneously buffing allied units while debuffing enemy ones. Can learn Geyser, Ink, Scald, and Mass Brainwash.",
            "Attacks land and air units.",
        )
    ),
    (
        "https://i.imgur.com/xnZ6PX3.png",
        (
            "Geyser: 125 Mana. 8 Second Cooldown.",
            "Causes a powerful geyser to erupt in the target area, launching enemy ground units into the air, disabling them for the duration they are airborne and dealing 80/120/160 damage. Units near the center are  damaged for 30/40/50% extra.",
            "Scald Bonus: Affected units are scalded, dealing an extra 0.5% bonus damage for each 1% health missing. The cooldown on Geyser resets if any of them are slain.",
        )
    ),
    (
        "https://i.imgur.com/EQCpCh5.png",
        (
            "Ink: 75 Mana. 8 Second Cooldown.",
            "Target enemy unit and up to 2 nearby enemy units are blasted with an ink spray which blinds them, causing attacks to miss for 3/4/5 seconds. If the target is a friendly unit, the effect instead heals them for 80/120/160 hit points.",
            "Scald Bonus: The effect reaches one additional unit. If the targets are enemies, the ink corrodes their armor, reducing it by 10 for the duration. If the targets are allies, they instead receive a 10 armor bonus for 3/4/5 seconds.",
        )
    ),
    (
        "https://i.imgur.com/4LwZSEn.png",
        (
            "Scald: 40/30/20 Mana. 3 Second Cooldown.",
            "Instant cast. Empowers the Sea Druid, causing its next action to have a bonus effect.",
            "If the next action is a basic attack, it deals 20/40/60% bonus damage and the victim is stunned for 0.1/0.3/0.5 seconds.",
        )
    ),
    (
        "https://i.imgur.com/ME7e8pn.png",
        (
            "Mass Brainwash: 140 Mana. 90 Second Cooldown.",
            "Sea Druid channels for up to 6 seconds, sapping the brains of enemy units within 900 range, progressively reducing their move speed and attack speed from 40% to 60%. If the Sea Druid completes channeling, he releases a psychic wave that empowers nearby allied units with 100% bonus attack speed for 5 seconds. When the channelling ceases, afflicted enemy units regain their speed over 6 seconds.",
            "Scald bonus: for the duration of the channelling, the Sea Druid creates a field of steam that pushes away enemies that get close to it.",
        )
    ),
)


def write(str):
    sys.stdout.write(str)


def print_section_header(title):
    write("[td][H3][color=#CCAA00]" + title + "[/color][/H3][/td][td][H3][color=#CCAA00]" + title + "[/color][/H3][/td]\n")


def get_paragraph(paragraph, embolden=True, color_numbers=True):
    p = paragraph.strip().replace('\n', ' ').replace('\t', '')
    res = textwrap.fill(p, width=90) + "\n\n"

    if embolden and re.match(r'[a-zA-Z ]+:', res):
        res = re.sub(r'^([a-zA-Z ]+:)', r'[b]\1[/b]', res)

    if color_numbers:
        res = re.sub(r'(\s[0-9\%\/\.]+\s)', r'[color=#ffcc00]\1[/color]', res)

    return res


def print_paragraph(paragraph):
    write(get_paragraph(paragraph))


def print_header():
    write("[CENTER]\n[TABLE]\n[CENTER]\n[H3][color=#60A600]")
    write(TEAM_NAME)
    write("[/color][/H3]\n[color=#CCAA00][B]")
    write(HERO_NAME)
    write("[/B][/color]\n\n[H3][color=#60A600]")
    write(AUTHORS)
    write("[/color][/H3][/CENTER]\n\n\n")


def print_introduction():
    if INTRODUCTION:
        for paragraph in INTRODUCTION:
            print_paragraph(paragraph)


def print_descriptor():
    if DESCRIPTOR:
        write("\n\n[otable]\n")

        write("[tr]\n[tdalt][img]" + DESCRIPTOR[0][0] + "[/img][/tdalt]\n[tdalt]")
        for paragraph in DESCRIPTOR[0][1][0:-1]:
            print_paragraph(paragraph)
        write(get_paragraph(DESCRIPTOR[0][1][-1])[0:-2])
        write("[/tdalt]\n[/tr]\n")

        print_section_header(" ")

        for section in DESCRIPTOR[1:]:
            write("[tr]\n[tdalt][img]" + section[0] + "[/img][/tdalt]\n[tdalt]")
            for paragraph in section[1][0:-1]:
                print_paragraph(paragraph)
            write(get_paragraph(section[1][-1])[0:-2])
            write("[/tdalt]\n[/tr]\n")

        write("[/otable]\n\n")


def print_footer():
    write("[/TABLE]\n[/CENTER]\n")


if __name__ == "__main__":
    print_header()
    print_introduction()
    print_descriptor()
    print_footer()
