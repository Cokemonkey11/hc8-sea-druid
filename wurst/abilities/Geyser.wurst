/**
Geyser is Sea Druid's Q ability. It's a targeted, small area of effect crowd
control ability with relatively high damage and mana cost. When augmented by
Scald, the damage potential increases dramatically and it becomes an execute.
*/
package Geyser
    import ClosureTimers
    import DamageType
    import Knockback3
    import RegisterEvents
    import SeaDruid
    import SoundHelper
    import Wurstunit

    // The raw stun ID for Stun effect, which disrupts channeling units. See
    // http://www.thehelper.net/threads/order-ids.148097/ for more information.
    constant ID_STUNNED = 851973

    constant RADIUS                        =  150.
    constant RADIUS_INNER                  =   50.
    constant DAMAGE_BASE                   =   40.
    constant DAMAGE_PER_LEVEL              =   40.

    // Not the height of the knockup, but rather, how low a unit must be to the
    // ground to be affected by Geyser. Note that flying units are exempt
    // regardless of flying height.
    constant HEIGHT_MAX_KNOCKUP            =   90.
    constant KNOCKUP_POWER_MAJOR           = 1250.
    constant KNOCKUP_POWER_MINOR           =  900.
    constant SCALD_FX_DURATION             =    3.
    constant INNER_RADIUS_FACTOR_BASE      =    1.2
    constant INNER_RADIUS_FACTOR_PER_LEVEL =     .1

    // When augmented by scald, what fraction of lost HP will be dealt as bonus
    // damage.
    constant SCALD_BONUS_RATE = 1. /  2.

    constant MINOR_KNOCKUP_ANGLE = angle((bj_PI / 2.) * (19. / 20.))

    constant SPOUT_EFFECT = "WaterGeyser.mdl"
    constant PATH_SOUND   = "Abilities\\Weapons\\WaterElementalMissile\\WaterElementalMissile1.wav"
    constant PATH_SOUND2  = "Abilities\\Spells\\Other\\CrushingWave\\CrushingWaveCaster1.wav"

    constant splashSound  = new Sound(PATH_SOUND,  1500, false, true)
    constant splashSound2 = new Sound(PATH_SOUND2, 2000, false, true)


    function calculateDamage(real baseDamage, real innerFactor, bool hadScald, real distFromCenter, real iterHpFrac) returns real
        var damage = baseDamage

        if distFromCenter < RADIUS_INNER
            damage *= innerFactor

        if hadScald
            damage *= 1. + (1. - iterHpFrac)*SCALD_BONUS_RATE

        return damage


    bool     hasSlain
    constant grp = CreateGroup()
    function action()
        let caster      = GetTriggerUnit()
        let owner       = caster.getOwner()
        let target      = vec2(GetSpellTargetX(), GetSpellTargetY())
        let level       = caster.getAbilityLevel(ID_GEYSER)
        let baseDamage  = DAMAGE_BASE + DAMAGE_PER_LEVEL*level.toReal()
        let innerFactor = INNER_RADIUS_FACTOR_BASE + INNER_RADIUS_FACTOR_PER_LEVEL*level.toReal()

        splashSound .playOnPoint(target.toVec3())
        splashSound2.playOnPoint(target.toVec3())
        addEffect(SPOUT_EFFECT, target).destr()

        let hadScald = seaDruidPopScald(caster)

        grp.enumUnitsInRange(target, RADIUS)
        while grp.hasNext()
            let iter = grp.next()

            let isEnemy   = iter.isEnemy(owner)
            let isAlive   = iter.isAlive()
            let notStruct = not IsUnitType(iter, UNIT_TYPE_STRUCTURE)
            let notImmune = not IsUnitType(iter, UNIT_TYPE_MAGIC_IMMUNE)
            let notFlying = not IsUnitType(iter, UNIT_TYPE_FLYING)
            let lowEnough = iter.getFlyHeight() < HEIGHT_MAX_KNOCKUP
            if isEnemy and isAlive and notStruct and notImmune and notFlying and lowEnough
                let iterPos        = iter.getPos()
                let distFromCenter = (target - iterPos).length()

                if distFromCenter < RADIUS_INNER
                    Knockback3.setVel(iter, KNOCKUP_POWER_MAJOR, angle(0.), angle(bj_PI / 2.))
                else
                    Knockback3.setVel(iter, KNOCKUP_POWER_MINOR, target.angleTo(iterPos), MINOR_KNOCKUP_ANGLE)

                iter.issueImmediateOrderById(ID_STUNNED)

                if hadScald
                    let fx = iter.addEffect(SCALD_EFFECT, SCALD_TARGET)
                    doAfter(SCALD_FX_DURATION, () -> begin
                        fx.destr()
                    end)

                let damage = calculateDamage(baseDamage, innerFactor, hadScald, distFromCenter, iter.getHP()/iter.getMaxHP())
                dealCodeDamage(caster, iter, damage)

                if not iter.isAlive()
                    hasSlain = true

        if hasSlain and hadScald
            // Reset geyser's cooldown if augmented by scald, and killed a unit.
            caster..removeAbility(ID_GEYSER)..addAbility(ID_GEYSER)


    init
        registerSpellEffectEvent(ID_GEYSER, function action)
        Knockback3.gravity = GEYSER_GRAVITY


    @test function maximumDamage()
        let distFromCenter = (vec2(0., 0.) - vec2(1., 0.)).length()
        let d = calculateDamage(160., 1.5, true,  distFromCenter, 0.)
        d.assertEquals((DAMAGE_BASE + DAMAGE_PER_LEVEL*3.) * (INNER_RADIUS_FACTOR_BASE + INNER_RADIUS_FACTOR_PER_LEVEL*3.) * (1. + SCALD_BONUS_RATE))


    @test function minimumDamage()
        let distFromCenter = (vec2(0., 0.) - vec2(RADIUS, 0.)).length()
        let d = calculateDamage( 80., 1.3, false, distFromCenter, 1.)
        d.assertEquals(DAMAGE_BASE + DAMAGE_PER_LEVEL)
